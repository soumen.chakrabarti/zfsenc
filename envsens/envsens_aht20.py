#! /usr/bin/python3

import os
import smbus
import time


def get_temp_humid_aht20(bus, address=0x38):
    bus.write_i2c_block_data(address, 0x00, [0xac, 0x33, 0x00])
    time.sleep(0.1)
    resp = bus.read_i2c_block_data(address, 0x00, 6)
    #resp = bus.read_i2c_block_data(address, 0x00, 7)
    humidity = resp[1]
    humidity <<= 8
    humidity += resp[2]
    humidity <<= 4
    humidity += (resp[3] >> 4)
    humidity /= 1048576.0
    humidity *= 100
    temperature = resp[3] & 0x0f
    temperature <<= 8
    temperature += resp[4]
    temperature <<= 8
    temperature += resp[5]
    temperature = temperature / 1048576.0*200.0-50.0  # Convert to Celsius
    return (temperature, humidity)


if __name__ == "__main__":
    bus = smbus.SMBus(1)
    address = 0x38
    tmpout = "/tmp/envsens_aht20.tsv"
    out = "/var/log/envsens_aht20.tsv"
    while True:
        try:
            now = int(time.time())
            (temp, humid) = get_temp_humid_aht20(bus, address)
            with open(tmpout, 'w') as file:
                file.write("{:d}\t{:.1f}\t{:.1f}\n".format(now, temp, humid))
            os.replace(tmpout, out)  # atomic file switcharoo
        except OSError:
            pass
        time.sleep(10)

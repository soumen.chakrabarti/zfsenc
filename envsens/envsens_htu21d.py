#! /usr/bin/python3

import os
import smbus
import time
import board
from adafruit_htu21d import HTU21D


if __name__ == "__main__":
    tmpout = "/tmp/envsens_htu21d.tsv"
    out = "/var/log/envsens_htu21d.tsv"
    i2c = board.I2C()  # uses board.SCL and board.SDA
    sensor = HTU21D(i2c)
    while True:
        try:
            now = int(time.time())
            (temp, humid) = (sensor.temperature, sensor.relative_humidity)
            with open(tmpout, 'w') as file:
                file.write("{:d}\t{:.1f}\t{:.1f}\n".format(now, temp, humid))
            os.replace(tmpout, out)  # atomic file switcharoo
        except OSError:
            pass
        time.sleep(10)
